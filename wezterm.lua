local wezterm = require 'wezterm';
-- The filled in variant of the < symbol
local SOLID_LEFT_ARROW = utf8.char(0xe0b2)

-- The filled in variant of the > symbol
local SOLID_RIGHT_ARROW = utf8.char(0xe0b0)
return {
  --window_decorations = "RESIZE",
  tab_bar_style = {
    active_tab_left = wezterm.format({
      {Background={Color="red"}},
      {Foreground={Color="#2b2042"}},
      {Text=SOLID_LEFT_ARROW},
    }),
    active_tab_right = wezterm.format({
      {Background={Color="red"}},
      {Foreground={Color="#2b2042"}},
      {Text=SOLID_RIGHT_ARROW},
    }),
    inactive_tab_left = wezterm.format({
      {Background={Color="#0b0022"}},
      {Foreground={Color="#1b1032"}},
      {Text=SOLID_LEFT_ARROW},
    }),
    inactive_tab_right = wezterm.format({
      {Background={Color="#0b0022"}},
      {Foreground={Color="#1b1032"}},
      {Text=SOLID_RIGHT_ARROW},
    }),
  },
   window_padding = {
    left = 7,
    -- This will become the scrollbar width if you have enabled the scrollbar!
    right = 7,

    top = 7,
    bottom = 0,
  },
  window_background_opacity = 0.051,
  font_size = 11.0,
  line_height = 1.1,
  freetype_load_target = "Light",
  freetype_render_target = "HorizontalLcd",
  --font = wezterm.font("Fira Code"),
  font = wezterm.font("JetBrainsMono Nerd Font Mono", {italic=false}),
  colors = {
      background = "#28343a",
      foreground = "#ffffff",
      tab_bar = {
        background = "#28343a",
        active_tab = {
          bg_color = "#666666",
          fg_color = "white",
        },
        inactive_tab = {
          bg_color = "#464646",
          fg_color = "#808080",

        -- The same options that were listed under the `active_tab` section above
        -- can also be used for `inactive_tab`.
        },
        new_tab = {
        bg_color = "#464646",
        fg_color = "#808080",

        -- The same options that were listed under the `active_tab` section above
        -- can also be used for `new_tab`.
        },
      },
  },
  color_scheme = "Andromeda",
  --colors = {
  --    background = "#28343a"
  --},
  --color_scheme = "Purple Rain",
  --color_scheme = "Solarized Dark Higher Contrast",
  --color_scheme = "Spacedust",
  --color_scheme = "ToyChest",
  --color_scheme = "Vaughn",
  exit_behavior = "Close",
  keys = {
    -- This will create a new split and run your default program inside it
    {key="6", mods="CTRL",action=wezterm.action{SplitHorizontal={domain="CurrentPaneDomain"}}},
    {key="8", mods="CTRL",action=wezterm.action{SplitVertical={domain="CurrentPaneDomain"}}},  }
}
